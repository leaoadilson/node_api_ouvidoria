const mongoose = require('mongoose');

const InformacaoSchema = new mongoose.Schema({
    nomeDenunciante:{
        type:String,
        required: true
    },
    contatoDenunciante:{
        type: String,
        required: true
    },
    emailDenunciante:{
        type: String,
        required: true
    },
    solicitacao:{
        type: String,
        required: true
    },
    criadaEm:{
        type: Date,
        default:Date.now
    },
});

mongoose.model('PedidoInformacao',InformacaoSchema)