const express = require('express');
const mongoose = require('mongoose');
const requiredDir = require('require-dir')
const bodyParser = require('body-parser');
const cors = require('cors');

//Iniciando o app
const app = express();
app.use(express.json())

//Iniciando o DB
mongoose.connect(
    'mongodb://localhost:27017/nodeapi',
    {
        useNewUrlParser: true,
        useUnifiedTopology: true
    }
);

const db = mongoose.connection;
  
db.on('connected', () => {
    console.log('Mongoose default connection is open');
});

db.on('error', err => {
    console.log(`Mongoose default connection has occured \n${err}`);
});

db.on('disconnected', () => {
    console.log('Mongoose default connection is disconnected');
});

process.on('SIGINT', () => {
    db.close(() => {
        console.log(
        'Mongoose default connection is disconnected due to application termination'
        );
        process.exit(0);
    });
});


requiredDir('./models')


//Rotas
app.use('/api', require('./routes'))

module.exports = app;