const mongoose = require('mongoose');

const PedidosInformacao = mongoose.model('PedidoInformacao')

module.exports = {
    async index(req, res){
        try {

            const pedido = await PedidosInformacao.find({});

            return res.status(200).json(pedido);

          } catch (e) {

            res.status(500).send({message: 'Falha ao buscar.'});
          }
        
    },
    async show(req, res){

        const pedido = await PedidosInformacao.findById(req.params.id);

        return res.json(pedido);
    },
    async store(req, res){
        try{

           const pedido = await PedidosInformacao.create(req.body);

           return res.status(201).json(pedido);

        } catch (e) {

            return res.status(500).send({message: 'Falha ao salvar Solicitação.'});

        }
        
    },
    async update(req, res){

        const pedido = await PedidosInformacao.findByIdAndUpdate(req.params.id,req.body,{new:true});

        return res.json(pedido);

    },
    async destroy(req, res){

        await PedidosInformacao.findByIdAndRemove(req.params.id);

        return res.send()
    },
}
