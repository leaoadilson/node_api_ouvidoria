const express = require('express');

const routes = express.Router();

const InformacaoController = require('./controllers/InformacaoController')

//Rotas - Pedidos de Informação
routes.get('/pedidos_de_informacao/',InformacaoController.index);
routes.get('/pedidos_de_informacao/:id',InformacaoController.show);
routes.post('/pedidos_de_informacao/',InformacaoController.store);
routes.put('/pedidos_de_informacao/:id',InformacaoController.update);
routes.delete('/pedidos_de_informacao/:id',InformacaoController.destroy);




module.exports = routes;